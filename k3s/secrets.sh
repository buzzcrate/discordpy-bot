#!/bin/bash
: <<EOC 
Creates a secrets file for the project to generated and applied to the project.
EOC

TOKEN64="$(echo $1|base64 -w0)"

cat >secrets.yaml <<EOL
---
apiVersion: v1
kind: Secret
metadata:
  name: discordtoken
  namespace: discord
type: Opaque
data:
  DISCORD_TOKEN: $TOKEN64
EOL

echo "secrets.yaml generated"
