from prettytable import PrettyTable
import requests
import json
import flag

lmaoApiUrl = "https://corona.lmao.ninja/v2/"

with open("states.json",'r') as statefile:
	statejson = json.load(statefile)

def novelOutput(name, region):
	outFormat = ""
	#separate all values and change any none/null values to 0
	outList = []
	outList.append(name)
	outList.append(region['cases'])
	outList.append(region['todayCases'])
	outList.append(region['deaths'])
	outList.append(region['todayDeaths'])
	outList.append(region['recovered'])
	outList.append(region['active'])
	outList.append(region['casesPerOneMillion'])
	outList.append(region['deathsPerOneMillion'])
	outList.append(region['tests'])	
	outList.append(region['testsPerOneMillion'])	
	try:
		outList.append(region['recovered'])
	except:
		outList.append(region['cases'] - region['active'])
	outList = [0 if x == None else x for x in outList]
	
	#return all values and add commas to number values
	outFormat = f"{outList[0]} Covid-19 Statistics\n"
	outFormat += "🤢 Total Cases: " + str(f"{outList[1]:n}")
	outFormat += " -- Today's Cases: " + str(f"{outList[2]:n}")
	outFormat += "  💀 Total Deaths: " + str(f"{outList[3]:n}")
	outFormat += " -- Today's Deaths: " + str(f"{outList[4]:n}")
	outFormat += "  😅 Total Recovered: " + str(f"{outList[5]:n}\n")
	outFormat += f"Active Cases: {outList[6]:n}   Cases/Million: {outList[7]:n}   Deaths/Million: {outList[8]}    "
	outFormat += f"Tests: {outList[9]:n}    Tests/Million: {outList[10]}"

	return outFormat

def countyOutput(region):
	outFormat = ""
	#separate all values and change any none/null values to 0
	outList = []
	outList.append(f"{region['county']} County, {region['province']}")
	outList.append(region['stats']['confirmed'])
	outList.append(region['stats']['deaths'])
	outList.append(region['stats']['recovered'])
	outList = [0 if x == None else x for x in outList]
	
	#return all values and add commas to number values
	outFormat = f"{outList[0]} Covid-19 Statistics\n"
	outFormat += "🤢 Total Cases: " + str(f"{outList[1]:n}")
	outFormat += "  💀 Total Deaths: " + str(f"{outList[2]:n}")
	outFormat += "  😅 Total Recovered: " + str(f"{outList[3]:n}")
	return outFormat

def getTopX(region, topx):
	if topx == "":
		topx = "5"

	# table output formatting
	topTable = PrettyTable()
	topTable.field_names = [
		region,
		"Total Cases",
		"Today's Cases",
		"Total Deaths",
		"Today's Deaths"]
	topTable.align[region] = "l"
	topTable.align["Total Cases"] = "r"
	topTable.align["Today's Cases"] = "r"
	topTable.align["Total Deaths"] = "r"
	topTable.align["Today's Deaths"] = "r"

	url = f"{lmaoApiUrl}{region}?sort=cases"
	top = requests.get(url).json()

	if region == "States":
		sRegion = "state"
	elif region == "Countries":
		sRegion = "country"

	for i in range(int(topx)):
		topTable.add_row([
			top[i][sRegion],
			f"{top[i]['cases']:n}",
			f"{top[i]['todayCases']:n}",
			f"{top[i]['deaths']:n}",
			f"{top[i]['todayDeaths']:n}"
			])


	topXoutmsg = f'```\n{topTable.get_string(title=f"Covid-19 Top {topx} {region} by total cases")}```'
	return topXoutmsg

def SelkieSmooth(region, topx):
	if topx == "":
		topx = "5"

	# table output formatting
	topTable = PrettyTable()
	topTable.field_names = [
		region,
		"Cases/Million",
		"Death/Million",
		"Active Cases"
		]
	topTable.align[region] = "l"
	topTable.align["Cases/Million"] = "r"
	topTable.align["Death/Million"] = "r"
	topTable.align["Active Cases"] = "r"

	url = f"{lmaoApiUrl}{region}?sort=casesPerOneMillion"
	top = requests.get(url).json()

	if region == "States":
		sRegion = "state"
	elif region == "Countries":
		sRegion = "country"

	for i in range(int(topx)):
		topTable.add_row([
			top[i][sRegion],
			f"{top[i]['casesPerOneMillion']:n}",
			f"{top[i]['deathsPerOneMillion']:n}",
			f"{top[i]['active']:n}",
			])


	topXoutmsg = f'```\n{topTable.get_string(title=f"Covid-19 Top {topx} {region} by total cases")}```'
	return topXoutmsg


def usStateStats(state):
	stateOut = ""
	if len(state) == 2:
		try:
			state = statejson['states'][(state[:2])]
		except:
			return f"{state} is not a valid state abbreviation.  Try again."

	url = lmaoApiUrl + "states/" + state
	try:
		i = requests.get(url).json()
		stateOut = novelOutput(i['state'],i)
	except:
		stateOut = state + " is not a valid US State name.  Try again."

	return stateOut

def usCountyStats(state, county):
	url = f"{lmaoApiUrl}jhucsse/counties/{county}"
	countyOut = ""
	getState = state.upper()
	try:
		getState = statejson['states'][(getState[:2])]
		i = requests.get(url).json()
		for c in i:
			if c['province'] == getState:
				countyOut = countyOutput(c)
	except:
		countyOut = f"{county} is not a valid county name.  Try again."
	return countyOut

def countryStats(country):
	countryOut = ""
	if country == "":
		url = lmaoApiUrl + "all"
		i = requests.get(url).json()
		countryOut = novelOutput("Global", i)
	else:
		try:
			url = lmaoApiUrl + "countries/" + country
			i = requests.get(url).json()
			countryOut = novelOutput(i['country'], i)
		except:
			countryOut = country + " is not a valid country name. Try again."
	return countryOut
